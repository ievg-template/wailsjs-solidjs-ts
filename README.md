<h1 align="center">wails-template-solid-ts</h1>

<p align="center">Wails 的模版，开箱即用的 Vite, Solid, TypeScript 并支持热重载</p>

## 使用模版

```bash
wails init -n my-wails-solid -t https://github.com/xijaja/wails-template-solid-ts
```

## 启动调试

在工程目录中执行 `wails dev` 即可启动。

如果你想在浏览器中调试，请在另一个终端进入 `frontend` 目录，然后执行 `npm run dev` ，前端开发服务器将在 http://localhost:34115 上运行。

## 前端UI 框架

[solid-ui](https://www.solid-ui.com/docs/introduction)

添加组件

```bash
npx solidui-cli@latest add [component]
```

路由还未添加， 如果添加会考虑使用官方的solid-router

## 构建

给你的项目打包，请执行命令： `wails build` 。
