import type { ParentProps, Component } from "solid-js";

export type SearchComponentProps<P = ParentProps> = P & {
  onSearch: (qs: string) => void;
};

export type SearchComponent<P = {}> = Component<SearchComponentProps<P>>;