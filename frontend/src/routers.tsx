import type { Component, ParentProps } from "solid-js";
import { Router, Route } from "@solidjs/router";


import Example from "./pages/example";

const Layout: Component<ParentProps> = (props) => {
  return <div class="container mx-auto">{props.children}</div>;
};

const Routers: Component = () => {
  return (
    <Router root={Layout}>
      <Route path="/" component={Example} />
    </Router>
  );
}

export default Routers;
