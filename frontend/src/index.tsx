/* @refresh reload */
import { render } from 'solid-js/web';

import Routers from './routers';

import '~/components/css/reset.css';
import '~/components/css/global.css';

render(() => <Routers />, document.getElementById('root') as HTMLElement);
